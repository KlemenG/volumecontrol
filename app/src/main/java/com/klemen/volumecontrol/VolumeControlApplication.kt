package com.klemen.volumecontrol

import android.app.Application
import timber.log.Timber

/**
 * Created by klemengabersek on 08/02/2022.
 */
@Suppress("unused")
class VolumeControlApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}