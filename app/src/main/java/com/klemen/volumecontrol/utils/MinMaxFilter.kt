package com.klemen.volumecontrol.utils

import android.text.InputFilter
import android.text.Spanned
import timber.log.Timber

/**
 * Created by klemengabersek on 08/02/2022.
 */
class MinMaxFilter(
    private val minValue: Int,
    private val maxValue: Int
) : InputFilter {
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dStart: Int,
        dEnd: Int
    ): CharSequence? {
        try {
            val input = Integer.parseInt(dest.toString() + source.toString())
            if (isInRange(minValue, maxValue, input)) {
                return null
            }
        } catch (e: NumberFormatException) {
            Timber.e(e)
        }
        return ""
    }

    private fun isInRange(a: Int, b: Int, c: Int): Boolean {
        return if (b > a) c in a..b else c in b..a
    }
}