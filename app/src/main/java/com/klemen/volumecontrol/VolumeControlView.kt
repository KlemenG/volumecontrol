package com.klemen.volumecontrol

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.window.layout.WindowMetricsCalculator
import java.util.*
import kotlin.math.roundToInt

/**
 * Created by klemengabersek on 08/02/2022.
 */
class VolumeControlView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr), View.OnTouchListener {
    private val greyPaint = Paint()
    private val coloredPaint = Paint()
    var percentageChangedCallback: ((percentage: Int) -> Unit)? = null

    @ColorInt
    var lineColor: Int
    private var volumePercentage: Int
    var lines: Int
    private var endX: Float
    private var startX: Float
    private var viewHeight: Int = 0
    private var lineHeight: Float
    private var maxLines: Int = 0

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.VolumeControlView,
            0, 0
        ).apply {
            try {
                lineColor = getColor(
                    R.styleable.VolumeControlView_line_color,
                    ContextCompat.getColor(context, R.color.purple_700)
                )
                volumePercentage = getInt(R.styleable.VolumeControlView_volume_percentage, 30)
                lines = getInt(R.styleable.VolumeControlView_lines, 5)
            } finally {
                recycle()
            }
        }

        lineHeight = resources.getDimension(R.dimen.volume_line_height)

        val windowMetrics = WindowMetricsCalculator.getOrCreate()
            .computeCurrentWindowMetrics(context as AppCompatActivity)
        val currentBounds = windowMetrics.bounds
        val screenWidth = currentBounds.width()

        endX = screenWidth - resources.getDimension(R.dimen.volume_control_margin)
        startX = screenWidth - endX

        greyPaint.strokeWidth = lineHeight
        greyPaint.style = Paint.Style.STROKE
        greyPaint.color = ContextCompat.getColor(context, R.color.boxColor)

        coloredPaint.strokeWidth = lineHeight
        coloredPaint.style = Paint.Style.STROKE
        coloredPaint.color = lineColor

        setOnTouchListener(this)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        percentageChangedCallback?.let { it(volumePercentage) }
        if (lines > maxLines) {
            lines = maxLines
        }
        val coloredLinesNum = ((lines * volumePercentage) / 100)
        val greyLinesNum = lines - coloredLinesNum
        for (i in 1..greyLinesNum) {
            canvas?.drawLine(startX, 100f * i, endX, 100f * i, greyPaint)
        }
        for (i in 1..coloredLinesNum) {
            canvas?.drawLine(
                startX, 100f * (i + greyLinesNum), endX, 100f * (i + greyLinesNum), coloredPaint
            )
        }
    }

    fun setLinesNumber(lines: Int) {
        this.lines = lines
        invalidate()
    }

    fun setVolumePercentage(volumePercentage: Int) {
        this.volumePercentage = volumePercentage
        invalidate()
    }

    fun setVolumeControlHeight(viewHeight: Int): Int {
        this.viewHeight = viewHeight
        maxLines = (viewHeight.px / (lineHeight + 100.px)).roundToInt()
        return maxLines
    }

    private val myListener = object : GestureDetector.SimpleOnGestureListener() {
        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent?,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            var newPercentage = (volumePercentage + (distanceY / 10)).roundToInt()
            if (newPercentage > 100) newPercentage = 100
            else if (newPercentage < 0) newPercentage = 0
            setVolumePercentage(newPercentage)
            return true
        }

        override fun onDoubleTap(e: MotionEvent?): Boolean {
            setLineColor()
            return true
        }
    }

    fun setLineColor() {
        val rnd = Random()
        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        lineColor = color
        coloredPaint.color = lineColor
        invalidate()
    }

    private val detector: GestureDetector = GestureDetector(context, myListener)

    override fun onTouch(p0: View?, event: MotionEvent?): Boolean {
        detector.onTouchEvent(event)
        return true
    }
}