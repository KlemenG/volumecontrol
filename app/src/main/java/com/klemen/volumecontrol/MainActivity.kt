package com.klemen.volumecontrol

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.InputFilter
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.doOnLayout
import com.klemen.volumecontrol.databinding.ActivityMainBinding
import com.klemen.volumecontrol.utils.MinMaxFilter

/**
 * Created by klemengabersek on 08/02/2022.
 */
class MainActivity : AppCompatActivity(), TextView.OnEditorActionListener {
    private lateinit var binding: ActivityMainBinding

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.volumeControlView.lineColor = ContextCompat.getColor(this, R.color.purple_700)

        binding.etVolume.filters = arrayOf<InputFilter>(MinMaxFilter(0, 100))

        binding.volumeControlView.doOnLayout { vcw ->
            binding.tvLines.text = "${binding.volumeControlView.lines}"
            val maxLines = binding.volumeControlView.setVolumeControlHeight(vcw.height)
            binding.etLines.filters = arrayOf<InputFilter>(MinMaxFilter(0, maxLines))
            binding.etLines.hint = "${getString(R.string.hint_set_lines)} (0-${maxLines})"
        }

        binding.etLines.setOnEditorActionListener(this)
        binding.etVolume.setOnEditorActionListener(this)

        binding.volumeControlView.percentageChangedCallback = {
            binding.tvPercentage.text = "$it %"
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            when (v) {
                binding.etLines -> {
                    val linesValue = binding.etLines.parseText()
                    if (linesValue.isNotEmpty()) {
                        binding.volumeControlView.setLinesNumber(linesValue.toInt())
                        binding.etLines.text.clear()
                        binding.tvLines.text = linesValue
                    }
                }
                binding.etVolume -> {
                    val volumeValue = binding.etVolume.parseText()
                    if (volumeValue.isNotEmpty()) {
                        binding.volumeControlView.setVolumePercentage(volumeValue.toInt())
                        binding.etVolume.text.clear()
                    }
                }
            }
        }
        return false
    }
}