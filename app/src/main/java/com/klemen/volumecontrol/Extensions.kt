package com.klemen.volumecontrol

import android.content.res.Resources
import android.widget.EditText

/**
 * Created by klemengabersek on 08/02/2022.
 */

fun EditText.parseText():String {
    return this.text.trim().toString()
}

val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

